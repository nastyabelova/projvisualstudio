﻿using exam_Belova.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exam_Belova
{
    public partial class Form1 : Form
    {
        Bitmap b,b1,b2,b3;
        Graphics g;
        Field field;
        private int[,] curFigure;
        private int numberFig = 0;
        private int curFig;
        private int curRowFig;
        private int curColFig;
        private bool figChecked = false;
        List<int[,]> lfig = new List<int[,]>();
        private int comp = 1;
        private bool gameContinue = true;

        public Point StartPoint { get; private set; }
        public float WidthCell { get; private set; }
        public float HeightCell { get; private set; }

        public Form1()
        {
            InitializeComponent();
            Block_puzzle block_Puzzle = new Block_puzzle();
            if (block_Puzzle.ShowDialog() == DialogResult.OK)
            {
            }

            Start dialog = new Start();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                comp = dialog.complexity;
                lfig = dialog.lfig;
            }
            if (comp == 1) label2.Text = $"Сложность: Легкая";
            if (comp == 2) label2.Text = $"Сложность: Средняя";
            if (comp == 3) label2.Text = $"Сложность: Сложная";
            if (comp == 4) label2.Text = $"Сложность: Пользовательская";

            button1.Click += Button1_Click;
            button2.Click += Button2_Click;

            pictureBox1.Paint += PictureBox1_Paint;
            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox1.MouseDown += PictureBox1_MouseDown;
            pictureBox1.MouseUp += PictureBox1_MouseUp;
            field = new Field(10,10,comp,lfig);
            label1.Text = $"Очки: {field.count}";

            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            b1 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
            b2 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
            b3 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
            
            DrawField();
            DrawFigures();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            field.Record();
            field = new Field(10, 10, comp,lfig);
            label1.Text = $"Очки:{field.count}";
            gameContinue = true;
            using (var g = Graphics.FromImage(b1))//очищение битмапа от фигур
            {
                g.Clear(DefaultBackColor);
                b1 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
            }
            using (var g = Graphics.FromImage(b2))
            {
                g.Clear(DefaultBackColor);
                b2 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
            }
            using (var g = Graphics.FromImage(b3))
            {
                g.Clear(DefaultBackColor);
                b3 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
            }
            DrawField();
            DrawFigures();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            List<string[]> records = new List<string[]>(); ;
            records = field.records;
            string rez = "";
            foreach(var item in records)
            {
                rez += $"{string.Join(":", item)}\n";
            }
            MessageBox.Show(rez);
        }

        private void PictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (gameContinue == false)
            {
                MessageBox.Show("Начните игру заново");
                MessageBox.Show("Начните игру заново");
                return;
            }

            label1.Text = $"Очки: {field.count}";
            figChecked = false;
            int x = e.X;
            int y = e.Y;
            var curRow = y / (int)HeightCell;
            var curCol = x / (int)WidthCell;
            if(y < pictureBox1.Height/3*2)
            {
                if (numberFig == 0) return;
                field.AddFigureToField(numberFig,curRow - curRowFig,curCol - curColFig);

                if (numberFig == 1)
                    using (var g = Graphics.FromImage(b1))
                    {
                        g.Clear(DefaultBackColor);
                        b1 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
                    }
                if (numberFig == 2)
                    using (var g = Graphics.FromImage(b2))
                    {
                        g.Clear(DefaultBackColor);
                        b2 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
                    }
                if (numberFig == 3)
                    using (var g = Graphics.FromImage(b3))
                    {
                        g.Clear(DefaultBackColor);
                        b3 = new Bitmap(pictureBox1.Width / 3, pictureBox1.Height / 3);
                    }

            }
                numberFig = 0;
               field.ChangeTemp();
                DrawField();
                DrawFigures();
            if (field.CheckLose())
            {
                field.Record();
                MessageBox.Show("Вы проиграли");
                gameContinue = false;
            }
        }

        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            var X = e.X;
            var Y = e.Y;
            var curRow = Y / (int)HeightCell;
            var curCol = X / (int)WidthCell;
            
            if (e.Button != MouseButtons.Left)
            {
                curRowFig = (Y - pictureBox1.Height / 3 * 2) / (pictureBox1.Height / 12);
                curColFig = (X / (pictureBox1.Width / 12) % 4);
                Text = $"{curRowFig}{curColFig}";
            }
            
            //Text = $"{curRow} {curCol} {curFig} {Y} {pictureBox1.Height / 3 * 2} {curRowFig} {curColFig}";
            //if (curRow > 10 || curCol > 10) return;
             
            if(Y > pictureBox1.Height/3*2)
            {
                    if (X < pictureBox1.Width / 3)
                        curFig = 1;
                    else if (X > pictureBox1.Width / 3 && X < pictureBox1.Width / 3 * 2)
                        curFig = 2;
                    else if (X > pictureBox1.Width / 3)
                        curFig = 3;
            }

            if (e.Button == MouseButtons.Left)
            {
                if (gameContinue == false)
                {
                    MessageBox.Show("Начните игру заново");
                    return;
                }

                if (figChecked == false)
                {
                    if (field.Check(curFig, curRowFig, curColFig))
                    {
                        if (Y > pictureBox1.Height / 3 * 2)//поле где фигуры стоят
                        {
                            if (figChecked == false)
                            {
                                figChecked = true;
                                if (X < pictureBox1.Width / 3)
                                    numberFig = 1;
                                else if (X > pictureBox1.Width / 3 && X < pictureBox1.Width / 3 * 2)
                                    numberFig = 2;
                                else if (X > pictureBox1.Width / 3)
                                    numberFig = 3;
                            }
                        }
                    }
                }
            }

            if (numberFig != 0)
            {
                field.ChangeTemp(curRow - curRowFig, curCol - curColFig, numberFig);
                using (g = Graphics.FromImage(b)) g.Clear(DefaultBackColor);
                DrawField();
                DrawFigures(numberFig, X, Y,curRowFig * (pictureBox1.Height / 12),curColFig * (pictureBox1.Width / 12)) ;
            }
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, 0, 0);
        }

        private void DrawFigures(int number = 0,int x = 0,int y = 0,int curRow = 0,int curCol = 0)
        {
            var width = pictureBox1.Width / 3 / 4;
            var height = pictureBox1.Height / 3 / 4;

            Picture1(width, height);
            Picture2(width, height);
            Picture3(width, height);

            using (var g = Graphics.FromImage(b))//одна фигура тянется за курсор
            {
                if (number == 0)
                {
                    g.DrawImage(b1, 0, pictureBox1.Height / 3 * 2);
                    g.DrawImage(b2, pictureBox1.Width / 3, pictureBox1.Height / 3 * 2);
                    g.DrawImage(b3, pictureBox1.Width / 3 * 2, pictureBox1.Height / 3 * 2);
                }
                else if (number == 1)
                {
                    g.DrawImage(b1, x - curCol, y - curRow);
                    g.DrawImage(b2, pictureBox1.Width / 3, pictureBox1.Height / 3 * 2);
                    g.DrawImage(b3, pictureBox1.Width / 3 * 2, pictureBox1.Height / 3 * 2);
                }
                else if (number == 2)
                {
                    g.DrawImage(b1, 0, pictureBox1.Height / 3 * 2);
                    g.DrawImage(b2, x - curCol, y - curRow);
                    g.DrawImage(b3, pictureBox1.Width / 3 * 2, pictureBox1.Height / 3 * 2);
                }
                else if (number == 3)
                {
                    g.DrawImage(b1, 0, pictureBox1.Height / 3 * 2);
                    g.DrawImage(b2, pictureBox1.Width / 3, pictureBox1.Height / 3 * 2);
                    g.DrawImage(b3, x - curCol, y - curRow); 
                }
            }
            pictureBox1.Invalidate();
        }

        private void Picture3(int width, int height)
        {
            var figure = field.GetFigure(3);
            var img = Resources.figure_blue;
            Bitmap l = new Bitmap(img, width, height);
            if (figure == null) return;
            using (var g = Graphics.FromImage(b3))
            {
                
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 4; j++)
                    {
                        if (figure[i, j] != 0)
                            g.DrawImage(l, j * width, i * height, width, height);
                    }
            }
        }

        private void Picture2(int width, int height)
        {
            var figure = field.GetFigure(2);
            var img = Resources.figure_blue;
            Bitmap l = new Bitmap(img, width, height);
            if (figure == null) return;
            using (var g = Graphics.FromImage(b2))
            {
                
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 4; j++)
                    {
                        if (figure[i, j] != 0)
                            g.DrawImage(l, j * width, i * height, width, height);
                    }
            }
        }

        private void Picture1(int width, int height)
        {
            var figure = field.GetFigure(1);
            var img = Resources.figure_blue;
            Bitmap l = new Bitmap(img, width, height);
            if (figure == null) return;
            using (var g = Graphics.FromImage(b1))
            {
                
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 4; j++)
                    {
                        if (figure[i, j] != 0)
                            g.DrawImage(l, j * width, i * height, width, height);
                    }
            }
        }

        private void DrawField()
        {
            WidthCell = (float)pictureBox1.Width / 10;
            HeightCell = (float)pictureBox1.Height/3 * 2 / 10;

            var pole = field.temp;
            var img = Resources.figure_blue;
            Bitmap l = new Bitmap(img, Width, Height);
            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int j = 0; j < 10; j++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), j * WidthCell, 0, j * WidthCell, pictureBox1.Height / 3 * 2);
                    g.DrawLine(new Pen(Color.Black, 1), 0, j * HeightCell, pictureBox1.Width, j * HeightCell);
                }
                for (int i = 0; i < 10; i++)
                    for (int j = 0; j < 10; j++)
                    {
                        if (pole[i, j] == 0)
                            continue;
                        if(pole[i,j] == 1)
                            g.DrawImage(l, j * WidthCell, i * HeightCell, WidthCell, HeightCell);
                        if (pole[i, j] == 2)
                            g.FillRectangle(new SolidBrush(Color.FromArgb(250 / 100 * 50, 160, 200, 0)),
                                j * WidthCell, i * HeightCell, WidthCell, HeightCell);
                    }
            }
            pictureBox1.Invalidate();
        }
    }
}
