﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exam_Belova
{

    public partial class Start : Form
    {
       public List<int[,]> lfig = new List<int[,]>();
        public int complexity;
        public Start()
        {
            InitializeComponent();
            button1.Click += (s, e) => complexity = 1;
            button2.Click += (s, e) => complexity = 2;
            button3.Click += (s, e) => complexity = 3;
            button4.Click += (s, e) => complexity = 4;
            button1.DialogResult = DialogResult.OK;
            button2.DialogResult = DialogResult.OK;
            button3.DialogResult = DialogResult.OK;
            button4.DialogResult = DialogResult.OK;
            button4.Click += Button4_Click;
        }

        public void Button4_Click(object sender, EventArgs e)
        {
                View view = new View();
                if (view.ShowDialog() == DialogResult.OK)
                {
                    lfig = view.rez;
                }
        }
    }
}
