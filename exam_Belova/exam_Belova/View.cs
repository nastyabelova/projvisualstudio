﻿using Aaaaaa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exam_Belova
{
    public partial class View : Form
    {
        List<int[,]> figures = new List<int[,]>();
        public List<int[,]> rez = new List<int[,]>();
        Bitmap b;
        public View()
        {

            InitializeComponent();

            ReadFigures();

            foreach (var item in figures)
                {
                    PictureBox pb = new PictureBox();
                    pb.Width = Width / 4;
                    pb.Height = Height / 6;
                    pb.Location = new Point((figures.IndexOf(item) % 4) * pb.Width, (figures.IndexOf(item) / 4) * pb.Height);
                    b = new Bitmap(pb.Width, pb.Height);
                    var width = pb.Width / 4;
                    var height = pb.Height / 4;

                    using (var g = Graphics.FromImage(b))
                    {
                        for (int i = 0; i < 4; i++)
                            for (int j = 0; j < 4; j++)
                            {
                                if (item[i, j] == 1)
                                    g.FillRectangle(new SolidBrush(Color.Coral), j * width, i * height, width, height);
                            }
                    }
                    pb.Image = b;
                    pb.Tag = figures.IndexOf(item);
                    pb.Click += Pb_Click;
                    Controls.Add(pb);
                }
                button1.DialogResult = DialogResult.OK;
                //button1.Click += Button1_Click;
                button2.Click += Button2_Click;
            }

        //private void Button1_Click(object sender, EventArgs e)
        //{
        //    //Form1 view = new Form1();
        //    //if (view.ShowDialog()==DialogResult.OK)
        //    //{ 
            
        //    //}
        //    //this.Close();
        //}

        private void Button2_Click(object sender, EventArgs e)
        {
            new_figure new_Figure = new new_figure();
            if (new_Figure.ShowDialog() == DialogResult.OK)
            { }
        }

        private void Pb_Click(object sender, EventArgs e)
        {
            if (sender is PictureBox pb)
            {
                if (rez.IndexOf(figures[int.Parse(pb.Tag.ToString())]) != -1)
                    rez.Remove(figures[int.Parse(pb.Tag.ToString())]);

                if (rez.IndexOf(figures[int.Parse(pb.Tag.ToString())]) == -1)
                    rez.Add(figures[int.Parse(pb.Tag.ToString())]);
            }
        }

        private void ReadFigures()
        {
            if (!File.Exists("Figures.txt"))
                File.Create("Figures.txt").Close();

            using (StreamReader sr = new StreamReader("Figures.txt"))
            {
                while (!sr.EndOfStream)
                {
                    int[,] mass = new int[4, 4];
                    for (int i = 0; i < 4; i++)
                    {
                        string line = sr.ReadLine();
                        for (int j = 0; j < 4; j++)
                        {
                            mass[i, j] = Convert.ToInt32(line[j].ToString());
                        }
                    }
                    sr.ReadLine();
                    figures.Add(mass);
                }
            }
        }
    }
}
