﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aaaaaa
{
    public partial class new_figure : Form
    {
        private Bitmap b;
        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }
        public int Cols { get; private set; } = 4;
        public int Rows { get; private set; } = 4;
        public int CurRow { get; private set; }
        public int Cursol { get; private set; }
        public int CurCol { get; private set; }
        int x1, y1;
        int[,] str_figure = new int[4, 4];
        public new_figure()
        { 
            InitializeComponent();
            ReseizeCells();
            DrawCells();
            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox1.Paint += PictureBox1_Paint;
            pictureBox1.MouseClick += PictureBox1_MouseClick;
            button1.Click += Bu_save_Click;
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    str_figure[i, j] = 0;
                }
            button2.DialogResult = DialogResult.OK;
        }

        private void Bu_clear_MouseClick(object sender, MouseEventArgs e)
        {
            ReseizeCells();
            DrawCells();
            pictureBox1.Paint += PictureBox1_Paint;

        }


        private void Bu_save_Click(object sender, EventArgs e)
        {
            if (!File.Exists("Figures.txt"))
                File.Create("Figures.txt").Close();
            using (StreamWriter streamWriter = new StreamWriter("Figures.txt", true))
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        streamWriter.Write(str_figure[i, j]);
                    }

                    streamWriter.Write(streamWriter.NewLine);
                }
                streamWriter.Write(streamWriter.NewLine);
            }
        }

        private void PictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            x1 = e.X;
            y1 = e.Y;
            Fill(e.X, e.Y);
            Add(x1, y1);

        }

        private void Add(int x1, int y1)
        {
            CurRow = y1 / CellHeight;
            CurCol = x1 / CellWidth;
            str_figure[CurRow, CurCol] = 1;
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            CurRow = e.Y / CellHeight;
            CurCol = e.X / CellWidth;
            Text = $"({CurRow},{CurCol})";
            DrawCells();
            pictureBox1.Invalidate();

        }

        public void Fill(int x, int y)
        {
            if (x >= pictureBox1.Width - 1) return;
            if (x < 1) return;
            if (y >= pictureBox1.Height - 1) return;
            if (y < 1) return;
            var g = Graphics.FromImage(b);
            g.DrawLine(new Pen(Color.Red), x, y, x, y + 0.5f);
            pictureBox1.Invalidate();
            if (b.GetPixel(x + 1, y).ToArgb() != Color.Black.ToArgb() && b.GetPixel(x + 1, y).ToArgb() != Color.Red.ToArgb())
            {
                Fill(x + 1, y);
            }
            if (b.GetPixel(x - 1, y).ToArgb() != Color.Black.ToArgb() && b.GetPixel(x - 1, y).ToArgb() != Color.Red.ToArgb())
            {
                Fill(x - 1, y);
            }
            if (b.GetPixel(x, y + 1).ToArgb() != Color.Black.ToArgb() && b.GetPixel(x, y + 1).ToArgb() != Color.Red.ToArgb())
            {
                Fill(x, y + 1);
            }
            if (b.GetPixel(x, y - 1).ToArgb() != Color.Black.ToArgb() && b.GetPixel(x, y - 1).ToArgb() != Color.Red.ToArgb())
            {
                Fill(x, y - 1);
            }
        }

        private void DrawCells()
        {
            using (var g = Graphics.FromImage(b))
            {
                for (int i = 0; i < Rows + 1; i++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), 0, i * CellHeight, Cols * CellWidth, i * CellHeight);
                }
                for (int j = 0; j < Cols + 1; j++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), j * CellWidth, 0, j * CellWidth, Rows * CellHeight);
                }
            }
        }

        private void ReseizeCells()
        {
            b = new Bitmap(Width, Height);
            CellWidth = pictureBox1.ClientSize.Width / Cols;
            CellHeight = pictureBox1.ClientSize.Height / Rows;
            DrawCells();
        }
    }
}
