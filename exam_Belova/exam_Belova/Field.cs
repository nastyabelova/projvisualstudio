﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exam_Belova
{
    class Field
    {
        int width, height;
        public int[,] field;
        Dictionary<int, int[,]> picbox = new Dictionary<int, int[,]>();
        public List<int[,]> lfig = new List<int[,]>();
        public int[,] temp;
        public int count;
        int complexity = 1; // уровни сложности от 1 до 4
        public List<string[]> records = new List<string[]>();
        int lastFigure = 0;
        public Field(int width,int height,int comp,List<int[,]> fig = null)
        {
            this.width = width;
            this.height = height;
            this.field = new int[width, height];
            this.temp = new int[width, height];
            this.complexity = comp;
            if(!(fig is null))
                this.lfig = fig;
            Record();
            if(comp != 4)
            {
                ReadFigures();
            }
            Start();
        }

        private void ReadFigures()
        {
            if (!File.Exists("Figures.txt"))
                File.Create("Figures.txt").Close();

            using(StreamReader sr = new StreamReader("Figures.txt"))
            {
                while (!sr.EndOfStream)
                {
                    int[,] mass = new int[4, 4];
                    for (int i = 0; i < 4; i++)
                    {
                        string line = sr.ReadLine();
                        for(int j = 0;j < 4;j++)
                        {
                            mass[i, j] = Convert.ToInt32(line[j].ToString());
                        }
                    }
                    sr.ReadLine();
                    lfig.Add(mass);
                }
            }
        }
        public bool CheckLose()//Проверка на проигрыш
        {
            List<int[,]> fig = new List<int[,]>();
            fig.Add(picbox[1]);
            fig.Add(picbox[2]);
            fig.Add(picbox[3]);
            int count = 0;
            if (picbox[1] is null && picbox[2] is null && picbox[3] is null) return false;
            foreach (var item in fig)
            {
                if (item is null)
                {
                    count++;
                    continue;
                }

                if (!CheckAddFigure(item))
                    count++;
            }
            if(count == 3)
                return true;
            return false;
        }

        private bool CheckAddFigure(int[,] item)//проверка на поражение
        {
            int t1, t2;
            bool flag;
            int count_ = 0;
            for(int i = 0;i < 10;i++)
            {
                for(int j = 0;j < 10;j++)
                {
                    flag = true;
                    t1 = 0;
                    for (int k = i; k < i + 4; k++)
                    {
                        t2 = 0;
                        for (int l = j; l < j + 4; l++)
                        {
                            if (item[t1, t2] == 1)
                            {
                                if (field[i, j] == 1)
                                {
                                    flag = false;
                                    count_++;
                                    break;
                                }
                            }
                            t2++;
                        }
                            if (flag == false) break;
                        t1++;
                    } 
                }
            }
            if (count_ == 100)
                return false;
            else return true;
        }

        private void Start()//начало игры
        {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    field[i, j] = 0;
                }
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    temp[i, j] = 0;
                }
                picbox[1] = SpawnFigure();
                picbox[2] = SpawnFigure();
                picbox[3] = SpawnFigure();
        }

        private int[,] SpawnFigure()//рандом фигур из файла
        {
            int a;
            if (complexity == 1)
            {
                a = new Random().Next(0, 6);
                while (a == lastFigure) a = new Random().Next(0, 6);
            }
            else if (complexity == 2)
            {
                a = new Random().Next(0, 8);
                while (a == lastFigure) a = new Random().Next(0, 8);
            }
            else if (complexity == 3)
            {
                a = new Random().Next(0, lfig.Count);
                while (a == lastFigure) a = new Random().Next(0, lfig.Count);
            }
            else
            {
                a = new Random().Next(0, lfig.Count);
                while (a == lastFigure) a = new Random().Next(0, lfig.Count);
            }
            lastFigure = a;
            return lfig[a];
        }

        //public int[,] ReturnFigure(int key)
        //{
        //    Random rnd = new Random();
        //    int[,] figure = new int[4, 4];

        //    if (picbox[key] is null)
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        figure = picbox[key];
        //    }
        //    return figure;
        //}

        private bool CheckFigures()//добавление 3 фигур, когда они встали на поле
        {
            bool flag = true;
            foreach(var key in picbox.Keys)
            {
                if(!(picbox[key] is null))
                {
                    flag = false;
                }
            }
            return flag;
        }
        private void CheckLines()//логика игры, исчезновение столбов- строк
        {
            int count_ = 0;
            for (int i = 0; i < 10; i++)
            {
                count_ = 0;
                for (int j = 0; j < 10; j++)
                {
                    if (field[i, j] == 0) break;
                    if (field[i, j] == 1) count_++;
                    if(count_ == 10)
                    {
                        for (int k = 0; k < 10; k++)
                            field[i, k] = 0;
                        count += 10;
                    }
                }
            }
            count_ = 0;
            for (int i = 0; i < 10; i++)
            {
                count_ = 0;
                for (int j = 0; j < 10; j++)
                {
                    if (field[j, i] == 0) break;
                    if (field[j, i] == 1) count_++;
                    if(count_ == 10)
                    {
                        for (int k = 0; k < 10; k++)
                            field[k, i] = 0;
                        count += 10;
                    }
                }
            }
        }
        
        public void ChangeTemp(int row=0,int col=0,int key=0)//формирование,где должнабыть тень
        {
            CheckLines();
            int t1 = 0;
            int t2 = 0;
            if (key ==0)
            {
                for(int i=0;i<width;i++)
                    for(int j=0;j<height;j++)
                    {
                        temp[i, j] = field[i, j];
                    }
                return;
            }
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    temp[i, j] = field[i, j];
                }
            try
            {
                for (int i = row; i < row + 4; i++)
                {
                    t2 = 0;
                    for (int j = col; j < col + 4; j++)
                    {
                        
                        if (picbox[key][t1, t2] == 1)
                        {
                            temp[i, j] = 2;
                        }
                        t2++;
                    }
                    t1++;
                }
            }
            catch (IndexOutOfRangeException)
            {
                for (int i = 0; i < width; i++)
                    for (int j = 0; j < height; j++)
                    {
                        temp[i, j] = field[i, j];
                    }
            }
        }
        public int[,] GetFigure(int key)//получение фигуры из pictBox
        {
            if (picbox[key] is null) return null;
            return picbox[key];
        }

        public void AddFigure(int key,int[,] figure)//добавитьфигуру
        {
            picbox[key] = figure;
        }

        public bool Check(int number,int row,int col)//проверка на какое место мышка попала в фигуре
        {
            if (row < 0 || col < 0)
                return false;
            var figure = picbox[number];
            if (figure[row, col] == 1)
                return true;
            return false;
        }
        public void AddFigureToField(int key,int row,int col)//добавление фигуры на поле
        {
            if (row > 10 || col > 10) return;
            CheckLines();
            int[,] figure = new int[4,4];
            int[,] temp1 = new int[10, 10];
            figure = picbox[key];
            picbox[key] = null;
            int t1 = 0;
            int t2 = 0;

            for(int i = 0;i<10;i++)
                for(int j =0;j < 10;j++)
                {
                    temp1[i, j] = field[i, j];
                }
            try
            {
                for (int i = row; i < row + 4; i++)
                {
                    t2 = 0;
                    for (int j = col; j < col + 4; j++)
                    {
                        if (figure[t1, t2++] == 1)
                        {
                            if (field[i, j] == 1)
                            {
                                picbox[key] = figure;
                                for (int k = 0; k < 10; k++)
                                    for (int l = 0; l < 10; l++)
                                    {
                                        field[k, l] = temp1[k, l];
                                        temp[k, l] = field[k, l];
                                    }
                                return;
                            }
                            field[i, j] = 1;
                        }
                    }
                    t1++;
                }
            }
            catch
            {
                picbox[key] = figure;
                for (int k = 0; k < 10; k++)
                    for (int l = 0; l < 10; l++)
                    {
                        field[k, l] = temp1[k, l];
                        temp[k, l] = field[k, l];
                    }
                return;
            }
                for (int i = 0; i < width; i++)
                    for (int j = 0; j < height; j++)
                    {
                        temp[i, j] = field[i, j];
                    }

                if (CheckFigures())
                {
                    picbox[1] = SpawnFigure();
                    picbox[2] = SpawnFigure();
                    picbox[3] = SpawnFigure();
                }
        }

        // запись рекордов в файл
        public void Record()
        {
            if(!File.Exists($"Record{complexity}.txt"))
                File.Create($"Record{complexity}.txt").Close();

            using(StreamReader sr = new StreamReader($"Record{complexity}.txt"))
            {
                while (!sr.EndOfStream)
                {
                    var record = sr.ReadLine();
                    records.Add(record.Split(':'));
                }
                records.Add(new string[] { "player", $"{count}" });
            }

            records = records.OrderByDescending(v => int.Parse(v[1])).ToList();

            while (records.Count > 10)
                records.RemoveAt(records.Count - 1);

            using(StreamWriter sw = new StreamWriter($"Record{complexity}.txt"))
            {
                foreach (var item in records)
                {
                    sw.WriteLine($"{string.Join(":", item)}");
                }
            }
        }
    }
}
