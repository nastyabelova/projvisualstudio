﻿namespace labRoadEditor
{
    partial class FormSize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TBWidth = new System.Windows.Forms.TextBox();
            this.TBHeight = new System.Windows.Forms.TextBox();
            this.buOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 100);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Высота";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 40);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ширина";
            // 
            // TBWidth
            // 
            this.TBWidth.Location = new System.Drawing.Point(112, 100);
            this.TBWidth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TBWidth.Name = "TBWidth";
            this.TBWidth.Size = new System.Drawing.Size(132, 22);
            this.TBWidth.TabIndex = 2;
            // 
            // TBHeight
            // 
            this.TBHeight.Location = new System.Drawing.Point(112, 40);
            this.TBHeight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TBHeight.Name = "TBHeight";
            this.TBHeight.Size = new System.Drawing.Size(132, 22);
            this.TBHeight.TabIndex = 3;
            // 
            // buOk
            // 
            this.buOk.Location = new System.Drawing.Point(85, 157);
            this.buOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buOk.Name = "buOk";
            this.buOk.Size = new System.Drawing.Size(100, 28);
            this.buOk.TabIndex = 4;
            this.buOk.Text = "Ok";
            this.buOk.UseVisualStyleBackColor = true;
            // 
            // FormSize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 198);
            this.Controls.Add(this.buOk);
            this.Controls.Add(this.TBHeight);
            this.Controls.Add(this.TBWidth);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormSize";
            this.Text = "FormSize";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBWidth;
        private System.Windows.Forms.TextBox TBHeight;
        private System.Windows.Forms.Button buOk;
    }
}