﻿namespace labRoadEditor
{
    partial class Fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TSM = new System.Windows.Forms.MenuStrip();
            this.TSMNewField = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMSave = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMClear = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM.SuspendLayout();
            this.SuspendLayout();
            // 
            // TSM
            // 
            this.TSM.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.TSM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMNewField,
            this.TSMClear,
            this.TSMSave});
            this.TSM.Location = new System.Drawing.Point(0, 0);
            this.TSM.Name = "TSM";
            this.TSM.Size = new System.Drawing.Size(1067, 28);
            this.TSM.TabIndex = 0;
            this.TSM.Text = "menuStrip1";
            // 
            // TSMNewField
            // 
            this.TSMNewField.Name = "TSMNewField";
            this.TSMNewField.Size = new System.Drawing.Size(106, 24);
            this.TSMNewField.Text = "Новое поле";
            // 
            // TSMSave
            // 
            this.TSMSave.Name = "TSMSave";
            this.TSMSave.Size = new System.Drawing.Size(97, 24);
            this.TSMSave.Text = "Сохранить";
            // 
            // TSMClear
            // 
            this.TSMClear.Name = "TSMClear";
            this.TSMClear.Size = new System.Drawing.Size(125, 24);
            this.TSMClear.Text = "Очистить поле";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.TSM);
            this.MainMenuStrip = this.TSM;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Fm";
            this.Text = "labRoadEditor";
            this.TSM.ResumeLayout(false);
            this.TSM.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip TSM;
        private System.Windows.Forms.ToolStripMenuItem TSMSave;
        private System.Windows.Forms.ToolStripMenuItem TSMNewField;
        private System.Windows.Forms.ToolStripMenuItem TSMClear;
    }
}

