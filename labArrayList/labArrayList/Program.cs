﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace labArrayList
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(3.14);
            list.Add(256);
            list.AddRange(new string[] { "one", "two" });

            foreach (object o in list) {
                Console.WriteLine(o);
            }
            Console.WriteLine();

            list.RemoveAt(0); //удалить первый элемент
            //list.reverse переворот списка
            // Console.writeLine(list[0]) получение элемента по списку
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
            Console.ReadLine();


        }
    }
}
