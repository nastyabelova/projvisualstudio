﻿using System;
using System.IO;

namespace labStreamWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\temp\tempSteam.txt";

            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                streamWriter.WriteLine("Один");
                streamWriter.WriteLine("Два");
                streamWriter.WriteLine("Три");
                Console.WriteLine("Файл создан");
            }

            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine("Четыре");
                Console.WriteLine("Файл дописан");
            }

            using (StreamReader streamReader = new StreamReader(path))
            {
                Console.WriteLine("---Начало---");
                Console.WriteLine(streamReader.ReadToEnd());
                Console.WriteLine("---Конец---");
                Console.WriteLine("Файл считал целиком");
            }

            using (StreamReader streamReader = new StreamReader(path))
            {
                string line;
                int n = 1;
                while ((line = streamReader.ReadLine()) != null)
                {
                    Console.WriteLine($"Строка {n++} = [{line}]");
                }
                Console.WriteLine("Файл считал построчно");
            }

            var a = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                foreach (var item in a)
                {
                    streamWriter.Write(item);
                }
                Console.WriteLine("Файл дозаписан ");
            }
        }
    }
}
