﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labDirToTags
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //edDir.
            //edTags

            /*
             Вход > путь к папке
             Выход > массив тегов с количеством их использования, отсортированный
             string[] tags = DirToTags("-----")
             */

            string s = @"D:\Фото\Мосполитех\2020.01.12 Семинар ООП (Иванов,Сидоров)\202001121300-002.jpg";
            var a = s.Split(new char[] { ' ','\\'}).ToArray();
            a = a.Select(v => v.TrimStart(new char[] { '(', '<' })
                            .TrimEnd(new char[] { ')','>'})).OrderBy(v => v).ToArray();

            edTags.Text = string.Join(Environment.NewLine, a);

        }
    }
}
