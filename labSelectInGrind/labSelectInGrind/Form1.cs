﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSelectInGrind
{
    public partial class Form1 : Form
    {
        private Bitmap b;

        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }
        public int Cols { get; private set; } = 4;
        public int Rows { get; private set; } = 3;
        public int CurRow { get; private set; }
        public int Cursol { get; private set; }
        public int CurCol { get; private set; }

        public Form1()
        {
            InitializeComponent();
            ReseizeCells();
            DrawCells();

            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox1.Paint += PictureBox1_Paint;
            pictureBox1.Resize += (s, e) => { ReseizeCells(); pictureBox1.Invalidate(); };
            numericUpDown_col.Minimum = 1;
            numericUpDown_row.Minimum = 1;
            numericUpDown_col.Maximum = 10;
            numericUpDown_row.Maximum = 10;
            Convert.ToInt32(numericUpDown_row.Value = Rows);
            Convert.ToInt32(numericUpDown_col.Value = Cols);
            numericUpDown_row.ValueChanged += (s, e) => { Rows = Convert.ToInt32(numericUpDown_row.Value); ReseizeCells(); pictureBox1.Invalidate(); };
            numericUpDown_col.ValueChanged += (s, e) => { Cols = Convert.ToInt32(numericUpDown_col.Value); ReseizeCells(); pictureBox1.Invalidate(); };
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point (0, 0));
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            CurRow = e.Y / CellHeight;
            CurCol = e.X / CellWidth;
            Text = $"({CurRow},{CurCol})";
            DrawCells();
            pictureBox1.Invalidate();
        }

        private void DrawCells()
        {
            //var g = Graphics.FromImage(b);
            //g.Dispose();

            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i < Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), 0, i * CellHeight, Cols * CellWidth, i * CellHeight);
                }
                for (int j = 0; j < Cols; j++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), j * CellWidth, 0, j * CellWidth, Rows * CellHeight);
                }
                //g.DrawString($"{CurRow},{CurCol}", new Font("",40), new SolidBrush(Color.Black),CurCol * CellWidth, CurRow * CellHeight);
                //выделить ту область, где находится курсор мыши
                g.DrawRectangle(new Pen(Color.Red, 3), CurCol * CellWidth, CurRow * CellHeight, CellWidth, CellHeight);
            }

        }

        private void ReseizeCells()
        {
            b = new Bitmap(Width, Height);
            CellWidth = pictureBox1.ClientSize.Width / Cols;
            CellHeight = pictureBox1.ClientSize.Width / Rows;
            DrawCells();
        }
    }
}
