﻿namespace labFileExplorerOnWB
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buDirSelect = new System.Windows.Forms.Button();
            this.edDir = new System.Windows.Forms.TextBox();
            this.buUp = new System.Windows.Forms.Button();
            this.buForward = new System.Windows.Forms.Button();
            this.buBack = new System.Windows.Forms.Button();
            this.WB = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buDirSelect);
            this.panel1.Controls.Add(this.edDir);
            this.panel1.Controls.Add(this.buUp);
            this.panel1.Controls.Add(this.buForward);
            this.panel1.Controls.Add(this.buBack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1067, 108);
            this.panel1.TabIndex = 0;
            // 
            // buDirSelect
            // 
            this.buDirSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buDirSelect.Location = new System.Drawing.Point(869, 37);
            this.buDirSelect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buDirSelect.Name = "buDirSelect";
            this.buDirSelect.Size = new System.Drawing.Size(100, 28);
            this.buDirSelect.TabIndex = 4;
            this.buDirSelect.Text = "обзор";
            this.buDirSelect.UseVisualStyleBackColor = true;
            // 
            // edDir
            // 
            this.edDir.Location = new System.Drawing.Point(357, 39);
            this.edDir.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(503, 22);
            this.edDir.TabIndex = 3;
            // 
            // buUp
            // 
            this.buUp.Location = new System.Drawing.Point(232, 37);
            this.buUp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(100, 28);
            this.buUp.TabIndex = 2;
            this.buUp.Text = "назад";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buForward
            // 
            this.buForward.Location = new System.Drawing.Point(124, 37);
            this.buForward.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(100, 28);
            this.buForward.TabIndex = 1;
            this.buForward.Text = "▶";
            this.buForward.UseVisualStyleBackColor = true;
            // 
            // buBack
            // 
            this.buBack.Location = new System.Drawing.Point(16, 37);
            this.buBack.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(100, 28);
            this.buBack.TabIndex = 0;
            this.buBack.Text = "◀";
            this.buBack.UseVisualStyleBackColor = true;
            // 
            // WB
            // 
            this.WB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WB.Location = new System.Drawing.Point(0, 108);
            this.WB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.WB.MinimumSize = new System.Drawing.Size(27, 25);
            this.WB.Name = "WB";
            this.WB.Size = new System.Drawing.Size(1067, 446);
            this.WB.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.WB);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "labFileExplorerOnWB";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buDirSelect;
        private System.Windows.Forms.TextBox edDir;
        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buForward;
        private System.Windows.Forms.Button buBack;
        private System.Windows.Forms.WebBrowser WB;
    }
}

