﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFormDialogs
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            buOpenFileDialog.Click += BuOpenFileDialog_Click;
        }

     
    }
    private void BuOpenFileDialog_Click(object sender, EventArgs e)
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        openFileDialog.InitialDirectory = edOpenFileDialog.Text;
        openFileDialog.Filter = "txt files (*.txt)|*.txt|all files (*.*)|*.*";
        if (OpenFileDialog.ShowDialog() == DialogResult.OK)
        {
            edOpenFileDialog.Text = openFileDialog.FileName;
        }
        else
        {
            edOpenFileDialog.Text = "";
        }
        
    }
}
