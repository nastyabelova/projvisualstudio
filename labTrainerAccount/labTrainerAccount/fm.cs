﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class fm : Form
    {
        private Games g;
        public fm()
        {
            InitializeComponent();
            g = new Games();
            g.Change += Event_Change;
            g.Reset();
            bCorrect.Click += delegate
            {
                g.doAnswer(true);
            };
            bIncorrect.Click += delegate {
                g.doAnswer(false);
            };

        }

        private void Event_Change(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Correct = {0}", g.CountCorrect.ToString());
            laIncorrect.Text = String.Format("Correct = {0}", g.CountIncorrect.ToString());
            laCode.Text = g.CodeText;
        }
    }
}
