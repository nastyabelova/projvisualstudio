﻿namespace EnglishLearn
{
    partial class CheckWords
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckWords));
            this.panel1 = new System.Windows.Forms.Panel();
            this.startBut = new System.Windows.Forms.Button();
            this.nextBut = new System.Windows.Forms.Button();
            this.checkBut = new System.Windows.Forms.Button();
            this.messageLb = new System.Windows.Forms.Label();
            this.outpLb = new System.Windows.Forms.Label();
            this.inpLb = new System.Windows.Forms.Label();
            this.input = new System.Windows.Forms.TextBox();
            this.output = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chT = new System.Windows.Forms.RadioButton();
            this.chEW = new System.Windows.Forms.RadioButton();
            this.backBut = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.modePanel = new System.Windows.Forms.Panel();
            this.inpBetw2 = new System.Windows.Forms.TextBox();
            this.inpRand = new System.Windows.Forms.TextBox();
            this.inpBetw1 = new System.Windows.Forms.TextBox();
            this.rBRandom = new System.Windows.Forms.RadioButton();
            this.rBBetween = new System.Windows.Forms.RadioButton();
            this.rBAll = new System.Windows.Forms.RadioButton();
            this.randLb = new System.Windows.Forms.Label();
            this.betwLb = new System.Windows.Forms.Label();
            this.infoPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mistakesLb = new System.Windows.Forms.Label();
            this.skippedLb = new System.Windows.Forms.Label();
            this.currectLb = new System.Windows.Forms.Label();
            this.wordsLb = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.modePanel.SuspendLayout();
            this.infoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.startBut);
            this.panel1.Controls.Add(this.nextBut);
            this.panel1.Controls.Add(this.checkBut);
            this.panel1.Controls.Add(this.messageLb);
            this.panel1.Controls.Add(this.outpLb);
            this.panel1.Controls.Add(this.inpLb);
            this.panel1.Controls.Add(this.input);
            this.panel1.Controls.Add(this.output);
            this.panel1.Controls.Add(this.label3);
            this.panel1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Location = new System.Drawing.Point(147, 55);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 320);
            this.panel1.TabIndex = 0;
            // 
            // startBut
            // 
            this.startBut.BackColor = System.Drawing.Color.Salmon;
            this.startBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startBut.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.startBut.Location = new System.Drawing.Point(200, 246);
            this.startBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.startBut.Name = "startBut";
            this.startBut.Size = new System.Drawing.Size(144, 48);
            this.startBut.TabIndex = 6;
            this.startBut.Text = "Start";
            this.startBut.UseVisualStyleBackColor = false;
            this.startBut.Click += new System.EventHandler(this.startBut_Click);
            // 
            // nextBut
            // 
            this.nextBut.BackColor = System.Drawing.Color.Salmon;
            this.nextBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nextBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextBut.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.nextBut.Location = new System.Drawing.Point(488, 246);
            this.nextBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nextBut.Name = "nextBut";
            this.nextBut.Size = new System.Drawing.Size(144, 48);
            this.nextBut.TabIndex = 6;
            this.nextBut.Text = "Next";
            this.nextBut.UseVisualStyleBackColor = false;
            this.nextBut.Visible = false;
            this.nextBut.Click += new System.EventHandler(this.nextBut_Click);
            // 
            // checkBut
            // 
            this.checkBut.BackColor = System.Drawing.Color.Tomato;
            this.checkBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBut.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBut.Location = new System.Drawing.Point(344, 246);
            this.checkBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBut.Name = "checkBut";
            this.checkBut.Size = new System.Drawing.Size(144, 48);
            this.checkBut.TabIndex = 6;
            this.checkBut.Text = "Check";
            this.checkBut.UseVisualStyleBackColor = false;
            this.checkBut.Visible = false;
            this.checkBut.Click += new System.EventHandler(this.checkBut_Click);
            // 
            // messageLb
            // 
            this.messageLb.AutoSize = true;
            this.messageLb.BackColor = System.Drawing.Color.Transparent;
            this.messageLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLb.ForeColor = System.Drawing.Color.Green;
            this.messageLb.Location = new System.Drawing.Point(629, 165);
            this.messageLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.messageLb.Name = "messageLb";
            this.messageLb.Size = new System.Drawing.Size(137, 52);
            this.messageLb.TabIndex = 11;
            this.messageLb.Text = "Right!";
            this.messageLb.Visible = false;
            // 
            // outpLb
            // 
            this.outpLb.AutoSize = true;
            this.outpLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outpLb.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.outpLb.Location = new System.Drawing.Point(31, 90);
            this.outpLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.outpLb.Name = "outpLb";
            this.outpLb.Size = new System.Drawing.Size(163, 36);
            this.outpLb.TabIndex = 11;
            this.outpLb.Text = "Translation";
            // 
            // inpLb
            // 
            this.inpLb.AutoSize = true;
            this.inpLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpLb.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.inpLb.Location = new System.Drawing.Point(31, 170);
            this.inpLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.inpLb.Name = "inpLb";
            this.inpLb.Size = new System.Drawing.Size(189, 36);
            this.inpLb.TabIndex = 9;
            this.inpLb.Text = "English word";
            // 
            // input
            // 
            this.input.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.input.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.input.Location = new System.Drawing.Point(259, 166);
            this.input.Margin = new System.Windows.Forms.Padding(0);
            this.input.MaxLength = 100;
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(333, 49);
            this.input.TabIndex = 10;
            // 
            // output
            // 
            this.output.BackColor = System.Drawing.SystemColors.ControlDark;
            this.output.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.output.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.output.Location = new System.Drawing.Point(259, 86);
            this.output.Margin = new System.Windows.Forms.Padding(0);
            this.output.MaxLength = 100;
            this.output.Name = "output";
            this.output.ReadOnly = true;
            this.output.Size = new System.Drawing.Size(333, 49);
            this.output.TabIndex = 8;
            this.output.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(225, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(334, 52);
            this.label3.TabIndex = 3;
            this.label3.Text = "Checking words";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.chT);
            this.panel2.Controls.Add(this.chEW);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Location = new System.Drawing.Point(684, 575);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(335, 86);
            this.panel2.TabIndex = 0;
            // 
            // chT
            // 
            this.chT.AutoSize = true;
            this.chT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chT.Location = new System.Drawing.Point(24, 47);
            this.chT.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chT.Name = "chT";
            this.chT.Size = new System.Drawing.Size(246, 35);
            this.chT.TabIndex = 1;
            this.chT.TabStop = true;
            this.chT.Text = "Check translation";
            this.chT.UseVisualStyleBackColor = true;
            this.chT.CheckedChanged += new System.EventHandler(this.chT_CheckedChanged);
            // 
            // chEW
            // 
            this.chEW.AutoSize = true;
            this.chEW.Checked = true;
            this.chEW.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chEW.Location = new System.Drawing.Point(24, 4);
            this.chEW.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chEW.Name = "chEW";
            this.chEW.Size = new System.Drawing.Size(289, 35);
            this.chEW.TabIndex = 0;
            this.chEW.TabStop = true;
            this.chEW.Text = "Check English words";
            this.chEW.UseVisualStyleBackColor = true;
            this.chEW.CheckedChanged += new System.EventHandler(this.chEW_CheckedChanged);
            // 
            // backBut
            // 
            this.backBut.BackColor = System.Drawing.Color.Gray;
            this.backBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBut.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.backBut.Location = new System.Drawing.Point(43, 597);
            this.backBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(159, 64);
            this.backBut.TabIndex = 6;
            this.backBut.Text = "Back";
            this.backBut.UseVisualStyleBackColor = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1067, 689);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // modePanel
            // 
            this.modePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.modePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modePanel.Controls.Add(this.inpBetw2);
            this.modePanel.Controls.Add(this.inpRand);
            this.modePanel.Controls.Add(this.inpBetw1);
            this.modePanel.Controls.Add(this.rBRandom);
            this.modePanel.Controls.Add(this.rBBetween);
            this.modePanel.Controls.Add(this.rBAll);
            this.modePanel.Controls.Add(this.randLb);
            this.modePanel.Controls.Add(this.betwLb);
            this.modePanel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.modePanel.Location = new System.Drawing.Point(280, 491);
            this.modePanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.modePanel.Name = "modePanel";
            this.modePanel.Size = new System.Drawing.Size(335, 167);
            this.modePanel.TabIndex = 0;
            // 
            // inpBetw2
            // 
            this.inpBetw2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpBetw2.Location = new System.Drawing.Point(276, 60);
            this.inpBetw2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.inpBetw2.Name = "inpBetw2";
            this.inpBetw2.ReadOnly = true;
            this.inpBetw2.Size = new System.Drawing.Size(48, 37);
            this.inpBetw2.TabIndex = 1;
            this.inpBetw2.Text = "2";
            this.inpBetw2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inpRand
            // 
            this.inpRand.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpRand.Location = new System.Drawing.Point(152, 119);
            this.inpRand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.inpRand.Name = "inpRand";
            this.inpRand.ReadOnly = true;
            this.inpRand.Size = new System.Drawing.Size(67, 37);
            this.inpRand.TabIndex = 1;
            this.inpRand.Text = "0";
            this.inpRand.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inpBetw1
            // 
            this.inpBetw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpBetw1.Location = new System.Drawing.Point(160, 60);
            this.inpBetw1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.inpBetw1.Name = "inpBetw1";
            this.inpBetw1.ReadOnly = true;
            this.inpBetw1.Size = new System.Drawing.Size(48, 37);
            this.inpBetw1.TabIndex = 1;
            this.inpBetw1.Text = "0";
            this.inpBetw1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rBRandom
            // 
            this.rBRandom.AutoSize = true;
            this.rBRandom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBRandom.Location = new System.Drawing.Point(11, 118);
            this.rBRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rBRandom.Name = "rBRandom";
            this.rBRandom.Size = new System.Drawing.Size(137, 35);
            this.rBRandom.TabIndex = 0;
            this.rBRandom.TabStop = true;
            this.rBRandom.Text = "Random";
            this.rBRandom.UseVisualStyleBackColor = true;
            this.rBRandom.CheckedChanged += new System.EventHandler(this.rBRandom_CheckedChanged);
            // 
            // rBBetween
            // 
            this.rBBetween.AutoSize = true;
            this.rBBetween.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBBetween.Location = new System.Drawing.Point(11, 62);
            this.rBBetween.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rBBetween.Name = "rBBetween";
            this.rBBetween.Size = new System.Drawing.Size(141, 35);
            this.rBBetween.TabIndex = 0;
            this.rBBetween.TabStop = true;
            this.rBBetween.Text = "Between";
            this.rBBetween.UseVisualStyleBackColor = true;
            this.rBBetween.CheckedChanged += new System.EventHandler(this.rBBetween_CheckedChanged);
            // 
            // rBAll
            // 
            this.rBAll.AutoSize = true;
            this.rBAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.rBAll.Checked = true;
            this.rBAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBAll.Location = new System.Drawing.Point(11, 9);
            this.rBAll.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rBAll.Name = "rBAll";
            this.rBAll.Size = new System.Drawing.Size(145, 35);
            this.rBAll.TabIndex = 0;
            this.rBAll.TabStop = true;
            this.rBAll.Text = "All words";
            this.rBAll.UseVisualStyleBackColor = false;
            this.rBAll.CheckedChanged += new System.EventHandler(this.rBAll_CheckedChanged);
            // 
            // randLb
            // 
            this.randLb.AutoSize = true;
            this.randLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.randLb.Location = new System.Drawing.Point(228, 123);
            this.randLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.randLb.Name = "randLb";
            this.randLb.Size = new System.Drawing.Size(87, 31);
            this.randLb.TabIndex = 9;
            this.randLb.Text = "words";
            // 
            // betwLb
            // 
            this.betwLb.AutoSize = true;
            this.betwLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.betwLb.Location = new System.Drawing.Point(212, 64);
            this.betwLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.betwLb.Name = "betwLb";
            this.betwLb.Size = new System.Drawing.Size(59, 31);
            this.betwLb.TabIndex = 9;
            this.betwLb.Text = "and";
            // 
            // infoPanel
            // 
            this.infoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.infoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.infoPanel.Controls.Add(this.label5);
            this.infoPanel.Controls.Add(this.label4);
            this.infoPanel.Controls.Add(this.label2);
            this.infoPanel.Controls.Add(this.mistakesLb);
            this.infoPanel.Controls.Add(this.skippedLb);
            this.infoPanel.Controls.Add(this.currectLb);
            this.infoPanel.Controls.Add(this.wordsLb);
            this.infoPanel.Controls.Add(this.label1);
            this.infoPanel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.infoPanel.Location = new System.Drawing.Point(748, 407);
            this.infoPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(209, 140);
            this.infoPanel.TabIndex = 0;
            this.infoPanel.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 101);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 31);
            this.label5.TabIndex = 9;
            this.label5.Text = "Skipped: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 70);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 31);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mistakes: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 31);
            this.label2.TabIndex = 9;
            this.label2.Text = "Currect: ";
            // 
            // mistakesLb
            // 
            this.mistakesLb.AutoSize = true;
            this.mistakesLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mistakesLb.Location = new System.Drawing.Point(149, 70);
            this.mistakesLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mistakesLb.Name = "mistakesLb";
            this.mistakesLb.Size = new System.Drawing.Size(23, 31);
            this.mistakesLb.TabIndex = 9;
            this.mistakesLb.Text = "-";
            // 
            // skippedLb
            // 
            this.skippedLb.AutoSize = true;
            this.skippedLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skippedLb.Location = new System.Drawing.Point(149, 101);
            this.skippedLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.skippedLb.Name = "skippedLb";
            this.skippedLb.Size = new System.Drawing.Size(23, 31);
            this.skippedLb.TabIndex = 9;
            this.skippedLb.Text = "-";
            // 
            // currectLb
            // 
            this.currectLb.AutoSize = true;
            this.currectLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currectLb.Location = new System.Drawing.Point(149, 39);
            this.currectLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currectLb.Name = "currectLb";
            this.currectLb.Size = new System.Drawing.Size(23, 31);
            this.currectLb.TabIndex = 9;
            this.currectLb.Text = "-";
            // 
            // wordsLb
            // 
            this.wordsLb.AutoSize = true;
            this.wordsLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wordsLb.Location = new System.Drawing.Point(149, 9);
            this.wordsLb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wordsLb.Name = "wordsLb";
            this.wordsLb.Size = new System.Drawing.Size(23, 31);
            this.wordsLb.TabIndex = 9;
            this.wordsLb.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 31);
            this.label1.TabIndex = 9;
            this.label1.Text = "Words: ";
            // 
            // CheckWords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.modePanel);
            this.Controls.Add(this.infoPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CheckWords";
            this.Size = new System.Drawing.Size(1067, 689);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.modePanel.ResumeLayout(false);
            this.modePanel.PerformLayout();
            this.infoPanel.ResumeLayout(false);
            this.infoPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button backBut;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button checkBut;
        private System.Windows.Forms.Label outpLb;
        private System.Windows.Forms.Label inpLb;
        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.TextBox output;
        private System.Windows.Forms.Button startBut;
        private System.Windows.Forms.RadioButton chT;
        private System.Windows.Forms.RadioButton chEW;
        private System.Windows.Forms.Button nextBut;
        private System.Windows.Forms.Label messageLb;
        private System.Windows.Forms.Panel modePanel;
        private System.Windows.Forms.TextBox inpBetw2;
        private System.Windows.Forms.TextBox inpRand;
        private System.Windows.Forms.TextBox inpBetw1;
        private System.Windows.Forms.RadioButton rBRandom;
        private System.Windows.Forms.RadioButton rBBetween;
        private System.Windows.Forms.RadioButton rBAll;
        private System.Windows.Forms.Label randLb;
        private System.Windows.Forms.Label betwLb;
        private System.Windows.Forms.Panel infoPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label mistakesLb;
        private System.Windows.Forms.Label skippedLb;
        private System.Windows.Forms.Label currectLb;
        private System.Windows.Forms.Label wordsLb;
    }
}
