﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labZoomImage
{
    public partial class Form1 : Form
    {
        public int DeltaZoom { get; private set; } = 50;

        public Form1()
        {
            InitializeComponent();

            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox2.Image = new Bitmap(pictureBox2.Width, pictureBox2.Height);
        }

        private void PictureBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            DeltaZoom += e.Delta > 0 ? 2 : -2;
            PictureBox1_MouseMove(sender, e);
        }
        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Text = $"({e.X},{e.Y})";
            var XPoint = e.Location;
            if (pictureBox1.SizeMode == PictureBoxSizeMode.Zoom)
            {
                XPoint.X = e.X * pictureBox1.Width / pictureBox1.Width;
                XPoint.Y = e.Y * pictureBox1.Height / pictureBox1.Height;
            }
            var g = Graphics.FromImage(pictureBox2.Image);
            g.DrawImage(pictureBox1.Image,
                new Rectangle(0, 0, pictureBox2.Width, pictureBox2.Height),
                //new Rectangle(XPoint.X - 50, XPoint.Y - 50, 100, 100),
                new Rectangle(XPoint.X - DeltaZoom,XPoint.Y - DeltaZoom, 
                DeltaZoom * 2, DeltaZoom * 2),
                GraphicsUnit.Pixel);
            g.Dispose();
            pictureBox2.Refresh();
        }
    }
}
