﻿using System;
using System.Collections.Generic;
using System.IO;

namespace labFile1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\temp\temp.txt";

            var list = new List<string>
            {
                "Один",
                "Два",
                "Три"
            };
            

            var xWriteText = string.Join(Environment.NewLine, list);
            File.WriteAllText(path, xWriteText);

            var xReadText = File.ReadAllText(path);
            Console.WriteLine(xReadText);
            Console.WriteLine();

            Console.WriteLine(File.Exists(path));
            Console.WriteLine();

            File.Delete(path);
        }
    }
}
