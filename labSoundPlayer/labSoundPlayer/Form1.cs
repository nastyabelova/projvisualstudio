﻿using labSoundPlayer.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSoundPlayer
{
    public partial class Form1 : Form
    {
        private SoundPlayer soundPlayer = new SoundPlayer();
        public Form1()
        {
            InitializeComponent();

            soundPlayer.Stream = Resources.sound_rain;

            button1.Click += (s, e) => soundPlayer.Play();
            button2.Click += (s, e) => soundPlayer.Stop();
            button3.Click += (s, e) => soundPlayer.PlayLooping();
            button4.Click += (s, e) => SystemSounds.Beep.Play();
        }
    }
}
