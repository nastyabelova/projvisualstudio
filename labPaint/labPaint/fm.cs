﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class fm : Form
    {
        public bool isPressed;
        private Image im;

        public fm()
        {
            InitializeComponent();

            im = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            pxImage.Paint += (s,e) => e.Graphics.DrawImage(im, new Point(0, 0));
            pxImage.MouseMove += PaImage_MouseMove;
            pxColor.MouseClick += PxColor_MouseClick;
            buSaveImage.Click += BuSaveImage_Click;
            buLoadImage.Click += BuLoadImage_Click;
        }

        private void BuLoadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files (*.BMP; *.JPG; *.GIF)| *.BMP; *.JPG; *.GIF|ALL files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                im = Bitmap.FromFile(dialog.FileName);
                pxImage.Invalidate();
            }
        }

        private void BuSaveImage_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPG Filtes (*.JPG|*.JPG)";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                im.Save(dialog.FileName); 
            }
        }

        private void PxColor_MouseClick(object sender, MouseEventArgs e)
        {
            ColorDialog colorDoalog = new ColorDialog();
            colorDoalog.Color = pxColor.BackColor;
            if (colorDoalog.ShowDialog() == DialogResult.OK)
                pxColor.BackColor = colorDoalog.Color;
        }

        private void PaImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var g = Graphics.FromImage(im);
                g.DrawLine(new Pen(pxColor.BackColor, 5), e.X - 10, e.Y - 10, e.X + 10, e.Y + 10);
                g.DrawLine(new Pen(pxColor.BackColor, 5), e.X - 10, e.Y + 10, e.X + 10, e.Y - 10);
                g.Dispose();
                pxImage.CreateGraphics().DrawImage(im, new Point(0, 0));
            }
        }
    }
}
