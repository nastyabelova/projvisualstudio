﻿namespace labPaint
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Pamenu = new System.Windows.Forms.Panel();
            this.pxColor = new System.Windows.Forms.PictureBox();
            this.buSaveImage = new System.Windows.Forms.Button();
            this.buLoadImage = new System.Windows.Forms.Button();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.Pamenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // Pamenu
            // 
            this.Pamenu.Controls.Add(this.buLoadImage);
            this.Pamenu.Controls.Add(this.buSaveImage);
            this.Pamenu.Controls.Add(this.pxColor);
            this.Pamenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.Pamenu.Location = new System.Drawing.Point(0, 0);
            this.Pamenu.Name = "Pamenu";
            this.Pamenu.Size = new System.Drawing.Size(200, 450);
            this.Pamenu.TabIndex = 0;
            // 
            // pxColor
            // 
            this.pxColor.BackColor = System.Drawing.Color.Yellow;
            this.pxColor.Location = new System.Drawing.Point(22, 28);
            this.pxColor.Name = "pxColor";
            this.pxColor.Size = new System.Drawing.Size(112, 99);
            this.pxColor.TabIndex = 0;
            this.pxColor.TabStop = false;
            // 
            // buSaveImage
            // 
            this.buSaveImage.Location = new System.Drawing.Point(22, 169);
            this.buSaveImage.Name = "buSaveImage";
            this.buSaveImage.Size = new System.Drawing.Size(112, 23);
            this.buSaveImage.TabIndex = 1;
            this.buSaveImage.Text = "SavetoFile";
            this.buSaveImage.UseVisualStyleBackColor = true;
            // 
            // buLoadImage
            // 
            this.buLoadImage.Location = new System.Drawing.Point(22, 218);
            this.buLoadImage.Name = "buLoadImage";
            this.buLoadImage.Size = new System.Drawing.Size(112, 23);
            this.buLoadImage.TabIndex = 2;
            this.buLoadImage.Text = "Load from file";
            this.buLoadImage.UseVisualStyleBackColor = true;
            // 
            // pxImage
            // 
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(200, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(600, 450);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.Pamenu);
            this.Name = "fm";
            this.Text = "Form1";
            this.Pamenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pxColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Pamenu;
        private System.Windows.Forms.Button buLoadImage;
        private System.Windows.Forms.Button buSaveImage;
        private System.Windows.Forms.PictureBox pxColor;
        private System.Windows.Forms.PictureBox pxImage;
    }
}

