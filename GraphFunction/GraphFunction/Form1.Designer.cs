﻿namespace labGraphicsOfFunction
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBox = new System.Windows.Forms.PictureBox();
            this.pan = new System.Windows.Forms.Panel();
            this.cbFunctions = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbA = new System.Windows.Forms.TrackBar();
            this.tbB = new System.Windows.Forms.TrackBar();
            this.tbC = new System.Windows.Forms.TrackBar();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.labelCoords = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            this.pan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbC)).BeginInit();
            this.SuspendLayout();
            // 
            // picBox
            // 
            this.picBox.BackColor = System.Drawing.Color.Snow;
            this.picBox.Location = new System.Drawing.Point(248, 27);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(738, 449);
            this.picBox.TabIndex = 0;
            this.picBox.TabStop = false;
            // 
            // pan
            // 
            this.pan.BackColor = System.Drawing.Color.Snow;
            this.pan.Controls.Add(this.labelCoords);
            this.pan.Controls.Add(this.labelC);
            this.pan.Controls.Add(this.labelB);
            this.pan.Controls.Add(this.labelA);
            this.pan.Controls.Add(this.tbC);
            this.pan.Controls.Add(this.tbB);
            this.pan.Controls.Add(this.tbA);
            this.pan.Controls.Add(this.textBox1);
            this.pan.Controls.Add(this.cbFunctions);
            this.pan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pan.Location = new System.Drawing.Point(0, 476);
            this.pan.Name = "pan";
            this.pan.Size = new System.Drawing.Size(1234, 82);
            this.pan.TabIndex = 1;
            // 
            // cbFunctions
            // 
            this.cbFunctions.FormattingEnabled = true;
            this.cbFunctions.Location = new System.Drawing.Point(217, 29);
            this.cbFunctions.Name = "cbFunctions";
            this.cbFunctions.Size = new System.Drawing.Size(196, 24);
            this.cbFunctions.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(29, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 22);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Выберите функцию";
            // 
            // tbA
            // 
            this.tbA.Location = new System.Drawing.Point(635, 14);
            this.tbA.Minimum = -10;
            this.tbA.Name = "tbA";
            this.tbA.Size = new System.Drawing.Size(199, 56);
            this.tbA.TabIndex = 2;
            this.tbA.Value = 1;
            // 
            // tbB
            // 
            this.tbB.Location = new System.Drawing.Point(860, 14);
            this.tbB.Minimum = -10;
            this.tbB.Name = "tbB";
            this.tbB.Size = new System.Drawing.Size(176, 56);
            this.tbB.TabIndex = 3;
            this.tbB.Value = 1;
            // 
            // tbC
            // 
            this.tbC.Location = new System.Drawing.Point(1067, 14);
            this.tbC.Minimum = -10;
            this.tbC.Name = "tbC";
            this.tbC.Size = new System.Drawing.Size(155, 56);
            this.tbC.TabIndex = 4;
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(629, 3);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(0, 17);
            this.labelA.TabIndex = 5;
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(854, 3);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(0, 17);
            this.labelB.TabIndex = 6;
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Location = new System.Drawing.Point(1061, 3);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(0, 17);
            this.labelC.TabIndex = 7;
            // 
            // labelCoords
            // 
            this.labelCoords.AutoSize = true;
            this.labelCoords.Location = new System.Drawing.Point(430, 29);
            this.labelCoords.Name = "labelCoords";
            this.labelCoords.Size = new System.Drawing.Size(46, 17);
            this.labelCoords.TabIndex = 8;
            this.labelCoords.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1234, 558);
            this.Controls.Add(this.pan);
            this.Controls.Add(this.picBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "GraphFunction";
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            this.pan.ResumeLayout(false);
            this.pan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picBox;
        private System.Windows.Forms.Panel pan;
        private System.Windows.Forms.TrackBar tbC;
        private System.Windows.Forms.TrackBar tbB;
        private System.Windows.Forms.TrackBar tbA;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cbFunctions;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelCoords;
    }
}

