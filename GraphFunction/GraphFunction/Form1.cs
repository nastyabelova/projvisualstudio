﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsOfFunction
{
    public partial class Form1 : Form
    {
        private Bitmap b;
        private Graphics g;
        private int height;
        private int width;
        private Stack<int> stackX = new Stack<int>();
        private Stack<int> stackY = new Stack<int>();
        private Pen myPen;

        public int CenterX { get; private set; }
        public int CenterY { get; private set; }

        public Form1()
        {
            InitializeComponent();

            DrawField();
            CreateSignals();

            labelCoords.Text = $"A={tbA.Value}      B={tbB.Value}       C={tbC.Value}";
            labelA.Text = $"A={tbA.Value}";
            labelB.Text = $"B={tbB.Value}";
            labelC.Text = $"C={tbC.Value}";
            string[] mass = 
                {
                  "y = bx + c",
                  "y=ax^2+bx+c",
                };
            cbFunctions.Text = "y=bx+c";
            cbFunctions.Items.AddRange(mass);

            myPen = new Pen(Color.Red,2);
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            DrawGraphics();
        }      
        private void DrawGraphics()
        {
            ShowBars();
            var function = cbFunctions.Text;
            var a = -tbA.Value;
            var b = tbB.Value;
            var c = tbC.Value;

            if (function == "y = bx + c")
            {
                DrawStrangthe(b, c);
            }

            if (function == "y=ax^2+bx+c")
                DrawParabola(a, b, c);

            picBox.Invalidate();
        }


        private void ShowBars()
        {
            labelA.Visible = true;
            labelB.Visible = true;
            labelC.Visible = true;

            tbA.Visible = true;
            tbB.Visible = true;
            tbC.Visible = true;

        }

        private void DrawStrangthe(int b, int c)
        {
            var x = -30;

            for (int i = 0; i < 100; i++)
            {
                var y = b * x + c;
                var x2 = x + 1;
                var y2 = b * x2 + c;
                g.DrawLine(myPen, x * 10 + CenterX, y * 10 + CenterY, x2 * 10 + CenterX, y2 * 10 + CenterY);
                x++;

            }
        }


        private void DrawParabola(int a,int b, int c)
        {
            var x = -20;

            for (int i = 0; i < 100; i++)
            {
                var y = a * x * x + b * x + c;
                var x2 = x + 1;
                var y2 = a * x2 * x2 + b * x2 + c;
                g.DrawLine(myPen, x * 10 + CenterX, y * 10 + CenterY, x2 * 10 + CenterX, y2 * 10 + CenterY);
                x++;

            }
        }

        private void CreateSignals()
        {
            picBox.MouseMove += (s, e) =>
            {
                this.Text = $"{ProductName}: x={e.X},y={e.Y}";
            };
            this.Resize += (s, e) =>
            {
                g.Clear(DefaultBackColor);
                DrawField();
                DrawGraphics();
            };
            picBox.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            tbA.ValueChanged += Tb_ValueChanged;
            tbB.ValueChanged += Tb_ValueChanged;
            tbC.ValueChanged += Tb_ValueChanged;
            cbFunctions.SelectedValueChanged += (s, e) =>
            {
                g.Clear(DefaultBackColor);
                DrawField();
                DrawGraphics();
            };
        }

        private void Tb_ValueChanged(object sender, EventArgs e)
        {
            labelCoords.Text = $"A={tbA.Value}      B={tbB.Value}       C={tbC.Value}";
            labelA.Text = $"A={tbA.Value}";
            labelB.Text = $"B={tbB.Value}";
            labelC.Text = $"C={tbC.Value}";

            g.Clear(DefaultBackColor);
            DrawField();
            DrawGraphics();
        }

        private void DrawField()
        {
             b = new Bitmap(picBox.Width, picBox.Height);
             g = Graphics.FromImage(b);
             g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            height = picBox.Height;
            width = picBox.Width;

            var height_ = picBox.Height / 30;
            var width_ = picBox.Width / 40;
            CenterX = width_ * 20;
            CenterY = height_ * 15;

            g.DrawLine(new Pen(Color.Black, 3), CenterX, 0, CenterX, height);
            g.DrawLine(new Pen(Color.Black, 3), 0, CenterY , width, CenterY);

            Pen p1 = new Pen(Color.Black, 2);
            Pen p2 = new Pen(Color.Gray, 1);
            Brush b1 = new SolidBrush(Color.Red);
            Brush b2 = new SolidBrush(Color.Black);
            Font f1 = new Font("Arial", 12);
            Font f2 = new Font("Arial", 10);

            g.DrawString("X", f1, b1, picBox.Width - 16, picBox.Height / 2 - 20);
            g.DrawString("Y", f1, b1, picBox.Width / 2, 0);

            int n = 1;
            for (int i = picBox.Width / 2 + 20; i < picBox.Width; i += 20)
            {
                g.DrawLine(p2, i, 0, i, picBox.Height);
                g.DrawString(n.ToString(), f2, b2, i, picBox.Height / 2);
                n++;
            }
            n = 0;
            for (int i = picBox.Width / 2 - 20; i > 0; i -= 20)
            {
                g.DrawLine(p2, i, 0, i, picBox.Height);
                g.DrawString("-" + n.ToString(), f2, b2, i, picBox.Height / 2);
                n++;
            }
            n = 1;
            for (int i = (picBox.Height) / 2 - 20; i > 0; i -= 20)
            {
                g.DrawLine(p2, 0, i, picBox.Width, i);
                g.DrawString(n.ToString(), f2, b2, picBox.Width / 2 - 15, i);
                n++;
            }
            n = 1;
            for (int i = (picBox.Height) / 2 + 20; i < picBox.Height; i += 20)
            {
                g.DrawLine(p2, 0, i, picBox.Width, i);
                g.DrawString("-" + n.ToString(), f2, b2, picBox.Width / 2 - 17, i);
                n++;
            }


            picBox.Invalidate();
        }
    }
}

